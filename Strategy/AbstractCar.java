package Strategy;

public abstract class AbstractCar extends AbstractVehicle {
	private GearboxStrategy gearboxStrategy;
	
	public AbstractCar(Engine engine) {
		super(engine);
		// More economical
		this.gearboxStrategy = new StandardGearboxStrategy();
	}
	
	public void setGearboxStrategy(GearboxStrategy gearboxStrategy) {
		this.gearboxStrategy = gearboxStrategy;
	}
	public GearboxStrategy getGearboxStrategy() {
        return getGearboxStrategy();
    }
	public void setSpeed(int speed) {
		this.gearboxStrategy.ensureCorrectGear(this.getEngine(), speed);
	}
}
