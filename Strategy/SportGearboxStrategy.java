package Strategy;

public class SportGearboxStrategy implements GearboxStrategy {

	@Override
	public void ensureCorrectGear(Engine engine, int speed) {
		// TODO Auto-generated method stub
		int engineSize = engine.getSize();
		boolean isTurbo = engine.isTurbo();
		// Some complicated code to determine correct gear
		System.out.println("working out correct at " + speed + " mph for SPORT gearbox");
	}

}
