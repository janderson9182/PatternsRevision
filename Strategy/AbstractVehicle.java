package Strategy;

public abstract class AbstractVehicle implements Vehicle {
	private Engine engine;
	public Engine getEngine() {
		return engine;
	}
	
	public AbstractVehicle (Engine engine) {
		this.engine = engine;
	}
}
