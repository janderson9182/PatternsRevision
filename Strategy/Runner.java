package Strategy;

public class Runner {
	public static void main(String args[]) {
		AbstractCar myCar = new Sport(new StandardEngine(2000, false));
		myCar.setSpeed(20);
		myCar.setSpeed(40);
		
		System.out.println("Switching to sport mode gearbox");
		myCar.setGearboxStrategy(new SportGearboxStrategy());
		myCar.setSpeed(20);
		myCar.setSpeed(40);
	}
}
