package Builder;

public class Runner {

	public static void main(String[] args) {
		CarDirector carDirector = new CarDirector();
		carDirector.build();
		
		VanDirector vanDirector = new VanDirector();
		vanDirector.build();
	}

}
