package Builder;

public class VanBuilder extends VehicleBuilder {

	@Override
	public void buildBody() {
		System.out.println("Buoilding van bofy");
		
	}

	@Override
	public void buildBoot() {
		System.out.println("Building van boot");
		
	}

	@Override
	public void buildChasis() {
		System.out.println("Building van chasis.");
		
	}

}
