package Builder;

public abstract class VehicleBuilder {
	public abstract void buildBody();
	public abstract void buildBoot();
	public abstract void buildChasis();
}
