package Builder;

public class CarBuilder extends VehicleBuilder {

	@Override
	public void buildBody() {
		System.out.println("Buolding Car Body");

	}

	@Override
	public void buildBoot() {
		System.out.println("Building car boot");

	}

	@Override
	public void buildChasis() {
		System.out.println("building car chasis");

	}

}
