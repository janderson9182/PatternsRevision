package Builder;

public class CarDirector extends VehicleDirector{

	@Override
	public void build() {
		VehicleBuilder builder = new CarBuilder();
		
		builder.buildBody();
		builder.buildBoot();
		builder.buildChasis();
	}

}
