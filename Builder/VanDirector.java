package Builder;

public class VanDirector extends VehicleDirector {

	@Override
	public void build() {
		VehicleBuilder builder = new VanBuilder();
		
		builder.buildBody();
		builder.buildBoot();
		builder.buildChasis();
	}

}
