package Builder;

public abstract class VehicleDirector {
	public abstract void build();
}
