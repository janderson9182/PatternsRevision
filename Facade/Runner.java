package Facade;

public class Runner {

	public static void main(String[] args) {
		Vehicle car = new FastCar();
		VehicleFacade facade = new VehicleFacade();
		facade.prepareForSale(car);
	}

}
