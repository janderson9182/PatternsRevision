package Facade;

public interface Vehicle {
	public void makeCar();
	public void paint();
	
	public void cleanInterior();
	public void cleanExterior();
	public void polishWindows();
	public void takeForTestDrive();
	
}
