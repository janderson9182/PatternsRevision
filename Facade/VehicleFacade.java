package Facade;

public class VehicleFacade {

	public VehicleFacade() {
	}
	
	public void prepareForSale(Vehicle vehicle) {
		Registration registration = new Registration(vehicle);
		registration.allocateVehicleNumber();
		registration.allocateLiscencePlate();
		
		Documentation.printBrochure();
		
		vehicle.cleanInterior();
		vehicle.cleanExterior();
		vehicle.polishWindows();
		vehicle.takeForTestDrive();
	}

}
