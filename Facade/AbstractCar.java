package Facade;

public abstract class AbstractCar implements Vehicle {

	public AbstractCar() {
	}

	@Override
	public void makeCar() {
		System.out.println("Car is made");
		
	}

	@Override
	public void paint() {
		System.out.println("Car is painted");
	}

	@Override
	public void cleanInterior() {
		System.out.println("Cleaning car interior");
		
	}

	@Override
	public void cleanExterior() {
		System.out.println("Cleaning car exterior");
	}

	@Override
	public void polishWindows() {
		System.out.println("Polishing windows");
	}

	@Override
	public void takeForTestDrive() {
		System.out.println("Taking for a test drive");
	}

}
