package Facade;

public class Registration {
	private Vehicle vehicle;
	public Registration(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	public void allocateLiscencePlate() {
		// Code omitted 
		System.out.println("Liscence plate allocated");
	}
	
	public void allocateVehicleNumber() {
		// code omitted
		System.out.println("Vehicle number allocated");
	}
	

}
