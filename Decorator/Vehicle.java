package Decorator;

public interface Vehicle {
	public enum Colour {
		UNPAINTED, 
		BLUE, 
		RED
	};
	public void paint(Vehicle.Colour colour);
	public Engine getEngine();
	public Vehicle.Colour getColour();
	public int getPrice();
}
