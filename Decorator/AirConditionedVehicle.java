package Decorator;

public class AirConditionedVehicle extends AbstractVehicleOption {

	public AirConditionedVehicle(Vehicle vehicle) {
		super(vehicle);
	}

	@Override
	public int getPrice() {
		return decoratedVehicle.getPrice() + 600;
	}
	
	public void setTemperature() {
		System.out.println("Setting temperature");
	}

}
