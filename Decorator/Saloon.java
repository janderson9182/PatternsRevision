package Decorator;

public class Saloon extends AbstractVehicle {

	public Saloon(Engine engine) {
		super(engine);
	}

	@Override
	public int getPrice() {
		return 600;
	}
}
