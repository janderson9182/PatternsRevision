package Decorator;

public abstract class AbstractVehicleOption extends AbstractVehicle {

	protected Vehicle decoratedVehicle;

	public AbstractVehicleOption(Vehicle vehicle) {
		super(vehicle.getEngine(),vehicle.getColour()); //keep colour
		decoratedVehicle = vehicle;
	}
}
