package Decorator;

public class Runner {

	public static void main(String[] args) {
		Vehicle myCar = new Saloon(new StandardEngine(200, false));
		myCar.paint(Vehicle.Colour.BLUE);
		myCar = new AirConditionedVehicle(myCar);
		System.out.println(myCar);
	}

}
