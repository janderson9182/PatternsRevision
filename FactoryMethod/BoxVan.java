package FactoryMethod;

public class BoxVan extends AbstractVan{

	@Override
	public void makeCar() {
		System.out.println(" I am a box van ");
	}

	@Override
	public void paint() {
		System.out.println("Painting box van");
	}

}
