package FactoryMethod;

public class FactoryMethodRunner {
		public static void main(String args[] ) {
			AbstractVehicleFactory carFactory = new CarFactory();
			
			carFactory.build(AbstractVehicleFactory.DrivingStyle.ECONOMICAL);
			carFactory.build(AbstractVehicleFactory.DrivingStyle.MIDRANGE);
			carFactory.build(AbstractVehicleFactory.DrivingStyle.POWERFUL);
			
			
			AbstractVehicleFactory vanFactory = new VanFactory();
			vanFactory.build(AbstractVehicleFactory.DrivingStyle.ECONOMICAL);
			vanFactory.build(AbstractVehicleFactory.DrivingStyle.MIDRANGE);
			vanFactory.build(AbstractVehicleFactory.DrivingStyle.POWERFUL);
		}
}
