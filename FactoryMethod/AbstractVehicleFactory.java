package FactoryMethod;

public abstract class AbstractVehicleFactory {
	public enum DrivingStyle{ECONOMICAL, MIDRANGE, POWERFUL};
	public Vehicle build(DrivingStyle style) {
		Vehicle vehicle = this.selectVehicle(style);
		vehicle.paint();
		return vehicle;
	}
	protected abstract Vehicle selectVehicle(DrivingStyle style);
}
