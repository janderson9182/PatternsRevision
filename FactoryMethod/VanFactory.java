package FactoryMethod;

public class VanFactory extends AbstractVehicleFactory{

	@Override
	protected Vehicle selectVehicle(DrivingStyle style) {
		if(style == DrivingStyle.ECONOMICAL)
		{
			return new BoxVan();
		}
		
		return new Pickip();
	}

}
