package FactoryMethod;

public class Coupe extends AbstractCar{

	@Override
	public void makeCar() {
		System.out.println("I am a cope");
	}

	@Override
	public void paint() {
		System.out.println("Painting coupe");
	}

}
