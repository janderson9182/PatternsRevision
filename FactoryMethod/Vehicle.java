package FactoryMethod;

public interface Vehicle {
	public void makeCar();
	public void paint();
}
