package FactoryMethod;

public class Sport extends AbstractCar{

	@Override
	public void makeCar() {
		System.out.println("I am a sport car");	
	}

	@Override
	public void paint() {
		System.out.println("Painting sport car");
	}
	
}
