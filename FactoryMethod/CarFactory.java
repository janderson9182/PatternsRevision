package FactoryMethod;

public class CarFactory extends AbstractVehicleFactory {

	@Override
	protected Vehicle selectVehicle(DrivingStyle style) {
		if(style == DrivingStyle.ECONOMICAL ) {
			return new Saloon();
		} else if(style == DrivingStyle.MIDRANGE) {
			return new Coupe();
		}
		return new Sport();
	}

}
