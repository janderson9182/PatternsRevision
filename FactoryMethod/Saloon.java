package FactoryMethod;

public class Saloon extends AbstractCar {

	@Override
	public void makeCar() {
		System.out.println("I am a saloon!");
	}

	@Override
	public void paint() {
		System.out.println("Painting saloon");
	}

}
