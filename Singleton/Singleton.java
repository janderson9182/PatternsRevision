package Singleton;

public class Singleton {
	private Singleton() {}
	private static Singleton singleton;
	
	private static int count = 0;

	public static Singleton getSingleton() {
		count ++;
		if(singleton == null)
		{
			singleton = new Singleton();
		}
		return singleton;
	}

	@Override
	public String toString() {
			return "Singleton number: " + count;
	}
	
	
}
