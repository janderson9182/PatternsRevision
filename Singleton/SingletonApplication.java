package Singleton;

public class SingletonApplication {
	public static void main(String args[])
	{
		/**
		 * Traditional singleton 
		 */
		for(int i = 0; i <= 5; i++)
		{
			System.out.println(Singleton.getSingleton());
		
		}
		/**
		 * ENUM Version
		 */
		System.out.println(EnumSingleton.INSTANCE.getNextSerial());
		System.out.println(EnumSingleton.INSTANCE.getNextSerial());
		System.out.println(EnumSingleton.INSTANCE.getNextSerial());
		System.out.println(EnumSingleton.INSTANCE.getNextSerial());
		System.out.println(EnumSingleton.INSTANCE.getNextSerial());
		
		/**
		 * The numbering starts for the multitoj 
		 */
		System.out.println(EnumSingleton.THISMAKESITAMULTITON.getNextSerial());
		System.out.println(EnumSingleton.THISMAKESITAMULTITON.getNextSerial());
		System.out.println(EnumSingleton.THISMAKESITAMULTITON.getNextSerial());
		System.out.println(EnumSingleton.THISMAKESITAMULTITON.getNextSerial());
		System.out.println(EnumSingleton.THISMAKESITAMULTITON.getNextSerial());
	}
}
