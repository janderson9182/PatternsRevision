package Singleton;

public enum EnumSingleton {
	INSTANCE, THISMAKESITAMULTITON;
	private int count = 0;
	public synchronized int getNextSerial()
	{
		return ++count;
	}
}
