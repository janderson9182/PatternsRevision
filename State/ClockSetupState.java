package State;

public interface ClockSetupState {
	public void previousValue();
	public void nextValue();
	public void selectValue();
	
	public String getInstructions();
	public int getSelectedValue();
}