package State;

import java.util.Calendar;

public class HourSetupState  implements ClockSetupState {
	private ClockSetup clockSetup;
	private int hour;
	public HourSetupState(ClockSetup clockSetup) {
		this.clockSetup = clockSetup;
		this.hour = Calendar.getInstance().get(Calendar.HOUR);
	}
	
	@Override
	public void previousValue() {
		hour --;
	}
	@Override
	public void nextValue() {
		hour ++;
	}
	@Override
	public void selectValue() {
		System.out.println("Setting hour to " + hour);
	}
	@Override
	public String getInstructions() {
		return "Please select the hour";
	}
	@Override
	public int getSelectedValue() {
		return hour;
	}
}
