package State;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ClockSetup {
	private ClockSetupState 
	yearState,
	monthState,
	dayState,
	hourState,
	minuteState,
	finishedState,
	currentState;
	
	public ClockSetup() {
		yearState = new YearSetupState(this);
		monthState = new MonthSetupState(this);
		dayState = new DaySetupState(this);
		hourState = new HourSetupState(this);
		minuteState = new MinuteSetupState(this);
		finishedState = new FinishedSetupState(this);
		setState(yearState);
	}
	
	public void setState(ClockSetupState state) {
		currentState = state;
		System.out.println(currentState.getInstructions());
	}
	
	public void rotateKnobLeft() {
		currentState.previousValue();
	}
	
	public void rotateKnobRight() {
		currentState.nextValue();
	}
	
	public void pushKnob() {
		currentState.selectValue();
	}
	
	public ClockSetupState getYearSetupState() {
		return yearState;
	}
	public ClockSetupState getMonthSetupState() {
		return monthState;
	}
	public ClockSetupState getDaySetupState() {
		return dayState;
	}
	public ClockSetupState getHourSetupState() {
		return hourState;
	}
	public ClockSetupState getMinuteSetupState() {
		return minuteState;
	}
	public ClockSetupState getFinishedSetupState() {
		return finishedState;
	}
	
	public Calendar getSelectedDate() {
		return new GregorianCalendar(
				yearState.getSelectedValue(),
				monthState.getSelectedValue(),
				dayState.getSelectedValue(),
				hourState.getSelectedValue(),
				minuteState.getSelectedValue()
				);
	}
}
