package State;

import java.util.Calendar;

public class DaySetupState implements ClockSetupState {
	private ClockSetup clockSetup;
	private int day;
	
	public DaySetupState(ClockSetup clockSetup) {
		this.clockSetup = clockSetup;
		this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}
	
	@Override
	public void previousValue() {
		day --;
	}

	@Override
	public void nextValue() {
		day ++;
	}

	@Override
	public void selectValue() {
		System.out.println("Setting the day to " + day);
		clockSetup.setState(clockSetup.getHourSetupState());
	}

	@Override
	public String getInstructions() {
		return "Please set the day of the month";
	}

	@Override
	public int getSelectedValue() {
		return day;
	}

}
