package State;

import java.util.Calendar;

public class MinuteSetupState implements ClockSetupState {
	private ClockSetup clockSetup;
	private int minute;
	
	public MinuteSetupState(ClockSetup clockSetup) {
		this.clockSetup = clockSetup;
		this.minute = Calendar.getInstance().get(Calendar.MINUTE);
	}
	
	@Override
	public void previousValue() {
		minute --;
	}

	@Override
	public void nextValue() {
		minute ++;
	}

	@Override
	public void selectValue() {
		System.out.println("Setting minute to " + minute);
		clockSetup.setState(clockSetup.getFinishedSetupState());
	}

	@Override
	public String getInstructions() {
		return "Please select the minute";
	}

	@Override
	public int getSelectedValue() {
		return minute;
	}

}
