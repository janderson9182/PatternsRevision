package State;

public class Runner {
	public static void main(String args[]) {
		ClockSetup clockSetup = new ClockSetup();

		clockSetup.rotateKnobRight();
		clockSetup.pushKnob(); 

		clockSetup.rotateKnobRight();
		clockSetup.rotateKnobRight();

		clockSetup.rotateKnobRight();
		clockSetup.rotateKnobRight();
		clockSetup.rotateKnobRight();
		clockSetup.pushKnob(); 


		clockSetup.rotateKnobLeft();
		clockSetup.rotateKnobLeft();
		clockSetup.pushKnob(); 

		clockSetup.rotateKnobRight();
		clockSetup.pushKnob(); 


		clockSetup.pushKnob();
	}
}
