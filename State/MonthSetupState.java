package State;

import java.util.Calendar;

public class MonthSetupState implements ClockSetupState {
	private ClockSetup clockSetup;
	private int month;
	
	public MonthSetupState(ClockSetup clockSetup) {
		this.clockSetup = clockSetup;
		month = Calendar.getInstance().get(Calendar.MONTH);
	}
	
	@Override
	public void previousValue() {
		month --;
	}

	@Override
	public void nextValue() {
		month ++;
	}

	@Override
	public void selectValue() {
		System.out.println("Set month to " + month);
		clockSetup.setState(clockSetup.getDaySetupState());
	}

	@Override
	public String getInstructions() {
		return "Please set the month";
	}

	@Override
	public int getSelectedValue() {
		return month;
	}

}
