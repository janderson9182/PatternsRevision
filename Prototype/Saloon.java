package Prototype;

public class Saloon extends AbstractVehicle {

	public Saloon(Engine engine) {
		super(engine);
	}

	@Override
	public void paint(Color color) {
		System.out.println("Painting Saloon " + color);
	}
	
}
