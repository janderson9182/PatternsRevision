package Prototype;

public abstract class AbstractVehicle implements Vehicle {
	private Engine engine;
	public Engine getEngine() {
		return engine;
	}

	private Vehicle.Color color;
	public Vehicle.Color getColor() {
		return color;
	}

	public AbstractVehicle (Engine engine) {
		this.engine = engine;
	}
	
	public AbstractVehicle (Engine engine, Vehicle.Color color) {
		this.engine = engine;
		this.color = color;
		// Lots of time consuming stuff here ...
	}
	
	public Object clone() {
		Object object = null;
		
		try {
			object = super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return object;
	}
	
}
