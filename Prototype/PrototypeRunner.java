package Prototype;

public class PrototypeRunner {

	public static void main(String[] args)
	{
		VehicleManager vehicleManager = new VehicleManager();

		vehicleManager.createCoupe().paint(Vehicle.Color.RED);
		
		vehicleManager.createSaloon().paint(Vehicle.Color.BLUE);

	}

}
