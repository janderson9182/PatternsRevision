package Prototype;

public class VehicleManager {
	private Vehicle 
	saloon, 
	coupe;

	public VehicleManager ()
	{
		// For simplicity, lets give all the vehicles the same engine
		saloon = new Saloon(new Engine(6000));
		
		coupe = new Coupe(new Engine(6000));
	}
	
	public Vehicle createSaloon()
	{
		return (Saloon) saloon.clone();
	}
	
	public Vehicle createCoupe()
	{
		return (Coupe) coupe.clone();
	}
}
