package Prototype;

public interface Vehicle extends Cloneable {
	public enum Color{UNPAINTED, RED, BLUE};
	public Engine getEngine();
	public Vehicle.Color getColor();
	public void paint(Vehicle.Color color);
	public Object clone();
}
