package SimpleFactory;

public class SimpleFactoryRunner {

	public static void main(String[] args) {
		GearboxFactory gearboxFactory = new GearboxFactory();
		
		Gearbox autoGearBox = gearboxFactory.create("Automatic");
		System.out.println(autoGearBox);
		
		Gearbox manualGearBox = gearboxFactory.create("Manual");
		System.out.println(manualGearBox);
	}

}
