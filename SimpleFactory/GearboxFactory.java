package SimpleFactory;

public class GearboxFactory {
	public Gearbox create(String typeWanted)
	{
		if(typeWanted.equals("Automatic"))
		{
			return new AutoGearbox();
		}
		return new ManualGearbox();
	}
}
