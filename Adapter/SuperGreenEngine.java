package Adapter;

public class SuperGreenEngine {
	private int size;
	public SuperGreenEngine(int size)
	{
		this.size = size;
	}
	
	public int getEngineSize()
	{
		return this.size;
	}
}
