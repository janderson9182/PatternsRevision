package Adapter;

import java.util.ArrayList;

public class AdapterRunner {
	public static void main(String args[]) {
		ArrayList<Engine> list = new ArrayList<>();
		
		list.add(new StandardEngine(300, false));
		list.add(new SportEngine(300, false));
		
		
		SuperGreenEngine superGreenEngine = new SuperGreenEngine(300);
		// Everyone forgets this bit!
		list.add(new SuperGreenEngineAdapter(superGreenEngine));
	}
}
