package Adapter;

public class SuperGreenEngineAdapter extends AbstractEngine{

	public SuperGreenEngineAdapter(SuperGreenEngine engine) {
		super(engine.getEngineSize(), false);
	}

}
