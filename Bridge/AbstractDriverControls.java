package Bridge;

public abstract class AbstractDriverControls {
	protected Engine engine;
	public AbstractDriverControls(Engine engine) {
		this.engine = engine;
	}
	public void ignitionOff() {
		engine.stop();
	}

	public void ignitionOn() {
		engine.start();
		
	}


	public void accelerate() {
		engine.increasePower();
	}


	public void brake() {
		engine.decreaserPower();
		
	}
}
