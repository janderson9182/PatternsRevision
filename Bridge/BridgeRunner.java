package Bridge;

public class BridgeRunner {
	public static void main(String args[]) {
		SportControls driverControls = new SportControls(new StandardEngine());
		driverControls.accelerate();
		driverControls.brake();
		driverControls.accelerateHard();
	}
}
