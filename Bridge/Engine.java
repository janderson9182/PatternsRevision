package Bridge;

public interface Engine {
	public void start();
	public void stop();
	public void increasePower();
	public void decreaserPower();
}
