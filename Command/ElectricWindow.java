package Command;

public class ElectricWindow {
	private boolean open;

	public ElectricWindow() {
		open = false;
		System.out.println("Window is closed");
	}
	public boolean isOpen() {
		return open;
	}
	public boolean isClosed() {
		return ! isOpen();
	}
	public void openWindow() {
		if(isClosed()) {
			open = true;
			System.out.println("window is now open");
		}
	}
	
	public void closeWindow() {
		if(isOpen()) {
			open = false;
			System.out.println("Window is now closed");
		}
	}
}
