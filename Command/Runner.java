package Command;

public class Runner {
	public static void main(String[] args) {
		Radio radio = new Radio();
		radio.on();
		Command volumeUp = new VolumeUpCommand(radio);
		volumeUp.execute();
		Command volumeDownCommand = new VolumeDownCommand(radio);
		volumeDownCommand.execute();
		
		SpeechRecogniser speechRecogniser = new SpeechRecogniser();
		speechRecogniser.setCommands(volumeUp, volumeDownCommand);
		
		System.out.println("Speach control controling the radio");
		speechRecogniser.hearUpSpoken();
		speechRecogniser.hearUpSpoken();
		speechRecogniser.hearUpSpoken();
		speechRecogniser.hearDownSpoken();
	}
}
