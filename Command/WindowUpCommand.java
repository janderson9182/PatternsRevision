package Command;

public class WindowUpCommand implements Command {
	private ElectricWindow window;
	public WindowUpCommand(ElectricWindow window) {
		this.window = window;
	}

	@Override
	public void execute() {
		window.openWindow();
	}

	@Override
	public void undo() {
		window.closeWindow();
	}

}
