package Command;

public class Radio {
	public static final int 
		MIN_VOLUME = 0, 
		MAX_VOLUME = 10, 
		DEFAULT_VOLUME = 5;
	
	private boolean on;
	private int volume;
	

	public Radio() {
		on = false;
		volume = DEFAULT_VOLUME;	
	}
	public boolean isOn() {
		return on;
	}
	
	public int getVolume() {
		return volume;
	}
	
	public void on() {
		on = true;
		System.out.println("Radio now on, volume level: " + volume);
	}
	
	public void off() {
		on = false;
		System.out.println("Radio now off.");
	}
	
	public void volumeUp() {
		if(isOn() && getVolume() < MAX_VOLUME) {
			volume ++;
			System.out.println("volume turned up to " + getVolume());
		}
	}
	
	public void volumeDown() {
		if(isOn() && getVolume() > MIN_VOLUME) {
			volume --;
			System.out.println("Volume turned down to level " + getVolume());
		}
	}
}
