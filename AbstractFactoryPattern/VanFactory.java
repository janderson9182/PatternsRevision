package AbstractFactoryPattern;

public class VanFactory implements AbstractVehicleFactory {
    @Override
    public void createBody() {
        new VanBoyd().getBodyParts();
    }

    @Override
    public void createWindows() {
        new VanWindow().getWindows();
    }

    @Override
    public void createChasis() {
        new VanChasis().getChasisParts();
    }
}
