package AbstractFactoryPattern;

public class VanBoyd implements Body {
    @Override
    public void getBodyParts() {
        System.out.println("Building van body");
    }
}
