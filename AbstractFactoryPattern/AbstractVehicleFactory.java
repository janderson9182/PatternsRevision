package AbstractFactoryPattern;

public interface AbstractVehicleFactory {
    public void createBody();
    public void createWindows();
    public void createChasis();
}
