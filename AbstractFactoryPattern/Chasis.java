package AbstractFactoryPattern;

public interface Chasis {
    public void getChasisParts();
}
