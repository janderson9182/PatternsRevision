package AbstractFactoryPattern;

public class CarFactory implements AbstractVehicleFactory{
    @Override
    public void createBody() {
        new CarBody().getBodyParts();
    }

    @Override
    public void createWindows() {
        new CarWindows().getWindows();
    }

    @Override
    public void createChasis() {
        new CarChasis().getChasisParts();
    }
}
