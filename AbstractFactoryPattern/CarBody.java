package AbstractFactoryPattern;

public class CarBody implements Body {
    @Override
    public void getBodyParts() {
        System.out.println("Building car body");
    }
}
