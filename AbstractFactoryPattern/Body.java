package AbstractFactoryPattern;

public interface Body {
    public void getBodyParts();
}
