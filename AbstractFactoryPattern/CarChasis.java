package AbstractFactoryPattern;

public class CarChasis implements Chasis {

    @Override
    public void getChasisParts() {
        System.out.println("Building car chasis");
    }
}
