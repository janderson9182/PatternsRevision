package AbstractFactoryPattern;

public class VanChasis implements Chasis {
    @Override
    public void getChasisParts() {
        System.out.println("Building van chasis");
    }
}
