package AbstractFactoryPattern;

public class Main {

    public static void main(String[] args) {
        abstractFactory();
    }

    private static void abstractFactory() {
        AbstractVehicleFactory carFactory = new CarFactory();
        carFactory.createBody();
        carFactory.createChasis();
        carFactory.createWindows();


        AbstractVehicleFactory vanFactory = new VanFactory();
        vanFactory.createBody();
        vanFactory.createChasis();
        vanFactory.createWindows();
    }
}
