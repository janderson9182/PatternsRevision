package AbstractFactoryPattern;

public interface Window {
    public void getWindows();
}
