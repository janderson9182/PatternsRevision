package Observer;

import java.awt.List;
import java.util.ArrayList;

public class Speedometer {
	private int currentSpeed;
	private ArrayList<SpeedometerListener> listeners;


	public Speedometer() {
		currentSpeed = 0;
		listeners = new ArrayList<SpeedometerListener>();
	}
	public void setCurrentSpeed(int speed) {
		currentSpeed = speed;
		fireSpeedChanged();
	}
	public int getCurrentSpeed() {
		return currentSpeed;
	}

	public void addSpeedometerListener(SpeedometerListener obj) {
		listeners.add(obj);
	}

	public void removeSpeedometerListener(SpeedometerListener obj) {
		listeners.remove(obj);
	}
	protected void fireSpeedChanged() {
		SpeedometerEvent speedEvent = new SpeedometerEvent(this, getCurrentSpeed());
		for (SpeedometerListener eachListener : listeners) {
			eachListener.speedChanged(speedEvent);
		}
	}
	
}
