package Observer;

import java.util.Observable;
import java.util.Observer;

public class SpeedMonitor implements SpeedometerListener {
	public static final int SPEED_TO_ALERT = 70;

	public SpeedMonitor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void speedChanged(SpeedometerEvent event) {
		if(	event.getSpeed() > SPEED_TO_ALERT ) {
			System.out.println("ALERT!! DRIVING TOO FAST!!! ( " + event.getSpeed() + "mph )");
		} else {
			System.out.println("Nice and steady ... ( " + event.getSpeed() + "mph )");
		}
	}
}