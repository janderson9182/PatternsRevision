package Observer;

public class Runner {

	public static void main(String args[]) {
		SpeedMonitor speedMonitor = new SpeedMonitor();
		
		Speedometer speedo = new Speedometer();
		speedo.addSpeedometerListener(speedMonitor);
		
		speedo.setCurrentSpeed(50);
		speedo.setCurrentSpeed(70);
		speedo.setCurrentSpeed(40);
		speedo.setCurrentSpeed(100);
		speedo.setCurrentSpeed(69);

	}

}
